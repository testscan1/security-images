# Video guide

[![Video guide](https://img.youtube.com/vi/DLN1kNh_Ha0/0.jpg)](https://www.youtube.com/watch?v=DLN1kNh_Ha0 "Video guide")

# Security Images for Pipelines

## Secret scanners
- Gitleaks
- Trufflehog3 (fork from trufflehog for dojo)

## Code scanners
- bandit
- brakeman
- eslint
- gosec
- retirejs
- semgrep
- sonarqube
- spotbugs
- hadolint
- terrascan
- gixy

## Code dependency scanners
- Trivy

## Image dependency scanners
- Trivy
- Grype

## Dynamic scanners
- Arachni
- OWASP ZAP

## Infrastructure scanners
- Subfinder
- Nuclei

