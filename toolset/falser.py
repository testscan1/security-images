from pprint import pprint
import requests
import os

defectdojo_url = os.environ.get('SEC_DD_URL', '')
defectdojo_key = os.environ.get('SEC_DD_KEY', '')
engagement = os.environ.get('DD_ENGAGEMENT_ID', '')
product_name = os.environ.get('DOJO_PROJECT_NAME', '')

# Edit this config to false issues with specific name in title AND tests in filepath
titles_if_tests_in_path = [
    'yaml load',
    'yaml.load',
    'SQL',
    'Jinja'
]


# Trufflehog3 makes too much similar findings for google key. It is 6 findings for one key :(
# Trufflehog3 finds entropy in svg files and html integrity fields (integrity="<here>")
patterns = {
    'entropy': [
        'svg',
        'go.sum',
        'go.mod',
        'package.json',
        'package-lock.json',
        'integrity',
        '.git',
        '.lock'
    ],
    'xss': [
        'go.sum',
        'go.mod',
        'package.json',
        'package-lock.json',
        '.git',
        '.lock'
    ],
    'Google': [
        'Drive API Key',
        'YouTube API Key',
        'Gmail API Key',
        'Cloud Platform'
    ]
}


def get_findings():
    offset = 0
    count = 10
    result = {
        'results': []
    }
    while len(result.get('results')) < count:
        response = requests.get(
            url=f'{defectdojo_url}/api/v2/findings/?duplicate=false&limit=100&static_finding=true'
                f'&verified=false&false_p=false&is_mitigated=false&test__engagement={engagement}&offset={offset}',
            headers={
                'Authorization': f'Token {defectdojo_key}'
            }
        ).json()
        count = response.get('count')
        result['results'].extend(response.get('results'))
        offset += 100
        print(f'Vulnerabilities ({offset}/{count})')
    return result


def mark_as_false(id):
    return requests.request(
        method='PATCH',
        url=f'{defectdojo_url}/api/v2/findings/{id}/',
        headers={
            'Authorization': f'Token {defectdojo_key}'
        },
        data={
          "verified": False,
          "active": False,
          "false_p": True,
        }
    )


def add_note(id, note):
    return requests.post(
        url=f'{defectdojo_url}/api/v2/findings/{id}/notes',
        headers={
            'Authorization': f'Token {defectdojo_key}'
        },
        data={
            "entry": f'{note}',
            "private": False,
            "note_type": 0
        }
    )


print('[Start] Getting vulnerabilities')
findings = get_findings()
print('[Done] Getting vulnerabilities')

changed = list()

print('[Start] Checking patterns')
for finding in findings.get('results'):
    # If tests in path goes with specific title
    if any(pattern in finding.get('title').lower() for pattern in titles_if_tests_in_path) \
            and 'tests' in finding.get('file_path'):
        status = mark_as_false(
            id=finding.get('id')
        ).status_code
        print(f'[{status}] Mark as FP')
        status = add_note(
            id=finding.get('id'),
            note='False Positive typical pattern'
        ).status_code
        print(f'[{status}] Adding Note')
        defectdojo_link = f'{defectdojo_url}/finding/{finding.get("id")}'
        changed.append(defectdojo_link)
        print(f'[FP No: {len(changed)}] {defectdojo_link}')

    # If description goes with specific title
    for title_value in patterns:
        for description_value in patterns.get(title_value):
            if title_value in finding.get('title').lower():
                if description_value in finding.get('title').lower():
                    status = mark_as_false(
                        id=finding.get('id')
                    ).status_code
                    print(f'[{status}] Mark as FP')
                    status = add_note(
                        id=finding.get('id'),
                        note='False Positive typical pattern'
                    ).status_code
                    print(f'[{status}] Adding Note')
                    defectdojo_link = f'{defectdojo_url}/finding/{finding.get("id")}'
                    changed.append(defectdojo_link)
                    print(f'[FP No: {len(changed)}] {defectdojo_link}')
print('[Done] Checking patterns')
pprint(changed)
